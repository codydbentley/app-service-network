package asn

import (
	"fmt"
	"io"
)

const ver = "0.1"

type OptFunc func(*Node)

type Node struct {
	asnp    string
	laddr   string
	raddr   int
	vendor  string
	version string
	nodes   map[*Node]int
	proxy   map[int]int
}

func NewNode(addr string, options ...OptFunc) (*Node, error) {
	node := Node{laddr: addr}
	for _, opt := range options {
		opt(&node)
	}
	return &node, nil
}

func OptAsnpVersion(version string) OptFunc {
	return func(node *Node) {
		node.asnp = version
	}
}

func OptVendor(vendor string) OptFunc {
	return func(node *Node) {
		node.vendor = vendor
	}
}

func OptVersion(version string) OptFunc {
	return func(node *Node) {
		node.version = version
	}
}

func (n *Node) Bootstrap(conn io.ReadWriteCloser) {
	if conn != nil {
		conn.Write([]byte("ANSP: " + ver + "\r\n\r\n"))
		conn.Write([]byte("addr: " + n.laddr + "\r\n\r\n"))
		conn.Write([]byte("vendor: " + n.vendor + "\r\n\r\n"))
		conn.Write([]byte("version: " + n.version + "\r\n\r\n"))
		conn.Write([]byte("CONNECT\r\n\r\n"))
		fmt.Fscanf(conn, "200 %d\r\n\r\n", &n.raddr)
	} else {
		n.raddr = 0
	}
}

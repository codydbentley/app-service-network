package main

import (
	"log"
	"time"

	"github.com/Microsoft/go-winio"
)

func main() {
	timeout := 10 * time.Second
	conn, err := winio.DialPipe(`\\.\pipe\asn-gateway`, &timeout)
	if err != nil {
		log.Fatalf("named pipe connection failed: %v", err)
	}
	defer conn.Close()

}

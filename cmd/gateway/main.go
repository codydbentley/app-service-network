package main

import (
	"fmt"
	"log"
	"net"

	"github.com/Microsoft/go-winio"
)

func main() {
	ln, err := winio.ListenPipe(`\\.\pipe\asn-gateway`, nil)
	if err != nil {
		log.Fatalf("failed to open named pipe: %v", err)
	}
	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("connection failed: %v", err)
			continue
		}
		go receive(conn)
	}
}

func receive(conn net.Conn) {
	c, err := handshake(conn)
	if err != nil {
		log.Printf("handshake error: %v\n", err)
		conn.Close()
		return
	}
	log.Printf("handshake success: %#v\n", c)
}

type client struct {
	conn net.Conn
	asnp string
	name string
	vend string
	vers string
}

func handshake(conn net.Conn) (*client, error) {
	c := client{}
	var maj, min int
	_, err := fmt.Fscanf(conn, "ASNP: %d.%d\r\n\r\n", &maj, &min)
	if err != nil {
		return nil, fmt.Errorf("asnp version scan failed: %v", err)
	}
	c.asnp = fmt.Sprintf("%d.%d", maj, min)
	_, err = fmt.Fscanf(conn, "CONNECT: %s\r\n\r\n", &c.name)
	if err != nil {
		return nil, fmt.Errorf("domain name scan failed: %v", err)
	}
	_, err = fmt.Fscanf(conn, "VENDOR: %s\r\n\r\n", &c.vend)
	if err != nil {
		return nil, fmt.Errorf("vendor scan failed: %v", err)
	}
	var pat int
	_, err = fmt.Fscanf(conn, "VERSION: %d.%d.%d\r\n\r\n", &maj, &min, &pat)
	if err != nil {
		return nil, fmt.Errorf("vendor version scan failed: %v", err)
	}
	c.vers = fmt.Sprintf("%d.%d.%d", maj, min, pat)
	_, err = fmt.Fscanf(conn, "READY\r\n\r\n")
	if err != nil {
		return nil, fmt.Errorf("ready scan failed: %v", err)
	}
	return &c, nil
}
